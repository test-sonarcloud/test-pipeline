FROM node:slim
ADD . .
RUN npm install
EXPOSE 2222
ENTRYPOINT [ "node app.js" ]